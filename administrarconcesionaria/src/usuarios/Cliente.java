/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;
import java.util.ArrayList;
import java.util.Scanner;
import sistema.*;
import vehiculos.*;
import menu.Main;

/**
 *
 * @author Carlos
 */
public class Cliente extends Usuario{
    static Scanner textoIngresado = new Scanner(System.in);
    //ATRIBUTOS
    private int noVehiculoComprado;
    private String cedula;
    private String ocupacion;
    private float ingresosMensuales;
    private ArrayList<ArrayList> solicitud = new ArrayList();
    
    
    //CONSTRUCTOR CON PARAMETROS
    public Cliente(){
        super("", "", "", "");
    }
    public Cliente(String user, String pass, String apellido, String nombre, int noVehiculoComprado, String cedula, String ocupacion, float ingresosMensuales){
        super(user, pass, apellido, nombre);
        this.noVehiculoComprado = noVehiculoComprado;
        this.cedula = cedula;
        this.ocupacion = ocupacion;
        this.ingresosMensuales = ingresosMensuales;
    }
    
    //GETTERS
    public int getNoVehiculoComprado(){
        return this.noVehiculoComprado;
    }
    public String getCedula(){
        return this.cedula;
    }
    public String getOcupacion(){
        return this.ocupacion;
    }
    public float getIngresosMensuales(){
        return this.ingresosMensuales;
    }
    public ArrayList<ArrayList> getSolicitud(){
        return this.solicitud;
    }
    
    //SETTERS
    public void setNoVehiculoComprado(int noVehiculoComprado){
        this.noVehiculoComprado = noVehiculoComprado;
    }
    public void setCedula(String cedula){
        this.cedula = cedula;
    }
    public void setOcupacion(String ocupacion){
        this.ocupacion = ocupacion;
    }
    public void setIngresosMensuales(float ingresosMensuales){
        this.ingresosMensuales = ingresosMensuales;
    }
    
    
    //METODOS
    @Override
    public String toString(){
        return "-----=====Cliente=====-----" + "\n" +
               "Usuario: " + this.getUsername() + ", " +
//               "Contraseña: " + this.getPassword() + ", " +
               "Apellidos: " + this.getApellido() + ", " +
               "Nombres: " + this.getNombre() + ", " + 
               "No. Vehículos Comprados: " + this.noVehiculoComprado + ", " + 
               "Cédula: " + this.cedula + ", " + 
               "Ocupación: " + this.ocupacion + ", " + 
               "Ingresos Mensuales: " + this.ingresosMensuales + 
               "\n";
    }//METODO QUE NOS PERMITIRA MOSTRAR UNICAMENTE AL USUARIO LOS VEHICULOS DISPONIBLES
    //YA QUE RECORRE TODOS LOS VEHICULOS COMPARANDO CUAL DE ELLOS SU ESTADO ES EXACTAMENTE
    //DISPONIBLE PARA ASI AGREGARLO A UNA LISTA Y POSTERIORMENTE MOSTRARLO
    @Override
    public ArrayList<Vehiculo> consultarStock(){
        ArrayList<Automovil> listAuto = SistemaConcesionaria.getListAutomovil();
        ArrayList<Motocicleta> listMoto = SistemaConcesionaria.getListMotocicleta();
        ArrayList<Camion> listCamion = SistemaConcesionaria.getListCamion();
        ArrayList<Tractor> listTractor = SistemaConcesionaria.getListTractor();
        ArrayList<String> vDisponible = new ArrayList();
        Automovil auto; Motocicleta moto; Camion camion; Tractor tractor;
        ArrayList<Vehiculo> listVehiculo = new ArrayList();
        
        for (int i = 0; i < listAuto.size(); i ++){
            auto = listAuto.get(i);
            if (auto.getEstado().equals("Disponible")){
                listVehiculo.add(auto);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Automóvil --> "
                        + "Marca: " + auto.getMarca() + ", " + "Modelo: " 
                        + auto.getModelo() + ", " + "Año Fabricación: " 
                        + auto.getAñoFabricacion()
                        + "\n");
            }
        }
        for (int i = 0; i < listMoto.size(); i ++){
            moto = listMoto.get(i);
            if (moto.getEstado().equals("Disponible")){
                listVehiculo.add(moto);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Motocicleta --> "
                        + "Marca: " + moto.getMarca() + ", " + "Modelo: " 
                        + moto.getModelo() + ", " + "Año Fabricación: " 
                        + moto.getAñoFabricacion()
                        + "\n");
            }
        }
        for (int i = 0; i < listCamion.size(); i ++){
            camion = listCamion.get(i);
            if (camion.getEstado().equals("Disponible")){
                listVehiculo.add(camion);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Camión --> " 
                        + "Marca: " + camion.getMarca() + ", " + "Modelo: " 
                        + camion.getModelo() + ", " + "Año Fabricación: " 
                        + camion.getAñoFabricacion()
                        + "\n");
            }
        }
        for (int i = 0; i < listTractor.size(); i ++){
            tractor = listTractor.get(i);
            if (tractor.getEstado().equals("Disponible")){
                listVehiculo.add(tractor);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Tractor --> "
                        + "Marca: " + tractor.getMarca() + ", " + "Modelo: " 
                        + tractor.getModelo() + ", " + "Año Fabricación: " 
                        + tractor.getAñoFabricacion()
                        + "\n");
            }
        }
        System.out.println(vDisponible);
        return listVehiculo;
    }
    
    public void solicitarCotizacion(Cliente cliente, Vendedor vendedor, Vehiculo vehiculo){
        ArrayList<Object> list = new ArrayList();
        list.add(cliente); list.add(vendedor); list.add(vehiculo); 
        vendedor.agregarSolicitud(list);
    }
    public void solicitarCompra(Cliente cliente, Supervisor supervisor, Vehiculo vehiculo){
        ArrayList<Object> list = new ArrayList();
        list.add(cliente); list.add(supervisor); list.add(vehiculo); 
        supervisor.agregarSolicitud(list);
    }
    public void solicitarMantenimiento(Cliente cliente, JefeTaller jefeTaller, Vehiculo vehiculo, String tpSolicitud, int km){
        ArrayList<Object> list = new ArrayList();
        list.add(cliente); list.add(jefeTaller); list.add(vehiculo); list.add(tpSolicitud); list.add(km);
        jefeTaller.agregarSoliVehiMantenimiento(list);
    }
    public void agregarSolicitud(String estado, String motivo, Vehiculo vehiculo, String tpSolicitud){
        ArrayList<Object> list = new ArrayList();
        list.add(estado); list.add(motivo); list.add(vehiculo); list.add(tpSolicitud);
        this.solicitud.add(list);
    }
    public void removerSolicitud(int id){
        this.solicitud.remove(id);
    }
    public static void procesoCotizar(Cliente cliente, ArrayList<Vehiculo> listVehiculo, String opc){
        int ind = Integer.parseInt(opc);
        ind = ind - 1;
        ArrayList<Vendedor> lis = SistemaConcesionaria.getListVendedor();
        int idVen = lis.size() - 1;
        int numero = (int) (Math.random() * idVen);
        Vendedor ven = lis.get(numero);
        
        cliente.solicitarCotizacion(cliente, ven, listVehiculo.get(ind));
        System.out.println("Solicitud Enviada!!!");
        Main.menuCliente();
    }
    public static void procesoComprar(Cliente cliente, Vehiculo vehiculo, String opc, int id){
        ArrayList<Supervisor> lis = SistemaConcesionaria.getListSupervisor();
        int idSup = lis.size() - 1;
        int numero = (int) (Math.random() * idSup);
        Supervisor sup = lis.get(numero);
        cliente.solicitarCompra(cliente, sup, vehiculo);
        cliente.removerSolicitud(id);
        System.out.println("Solicitud Enviada!!!");
        Main.menuCliente();
    }
    public static void procesoMantenimiento(Cliente cliente, ArrayList<ArrayList> listaSolicitudes, String tpMantenimiento, int opc, int km){
        ArrayList<Object> list = new ArrayList();
        list = listaSolicitudes.get(opc);
        JefeTaller jT = SistemaConcesionaria.getListJefeTaller().get(0);
        Vehiculo v = new Vehiculo("", "", 0, "", "", 0, 0);
        for (int i = 0; i < list.size(); i++){
            if (i == 2){
                v = (Vehiculo)list.get(2);
            }
        }
        cliente.solicitarMantenimiento(cliente, jT, v, tpMantenimiento, km);
        System.out.println("Solicitud Enviada!!!");
        Main.menuCliente();
    }
    //REALIZA LAS ACCIONES QUE EL USUARIO LOGRA VER MEDIANTE EL MENU DEMOSTRATIVO
    public static void procesoCliente(String opc, Cliente cliente){
        boolean valopc = true;
        while(valopc){
            if (Main.validarOpcion(opc)){
                //CONSULTA LOS VEHICULOS DISPONIBLES
                if (opc.equals("1")){
                    valopc = false;
                    Cliente c = new Cliente();
                    ArrayList<Vehiculo> listVehiculo = c.consultarStock();
                    boolean valopcAux = true;
                    String opcAux;
                    if (listVehiculo.size() != 0){
                        while(valopcAux){
                            System.out.print("--------------------------------------------------" + "\n"
                            + "1.- Cotizar" + "\n" 
                            + "2.- Regresar" + "\n" 
                            + "--------------------------------------------------" + "\n"
                            + "Por favor, digite el número de la acción que desea realizar: ");
                            opcAux = textoIngresado.nextLine();
                            if (Main.validarOpcion(opcAux) == true){
                                if (opcAux.equals("1")){
                                    valopcAux = false;
                                    String opcV;
                                    boolean valopcAux1 = true;
                                    while(valopcAux1){
                                        System.out.print("De la lista de vehículos, digite el número del que desea cotizar: ");
                                        opcV = textoIngresado.nextLine();
                                        if (Main.validarOpcion(opcV) == true){
                                            int op = Integer.parseInt(opcV);
    //                                        System.out.println("el" + op + "-" + listVehiculo.size());
                                            if(op <= listVehiculo.size()){
                                                valopcAux1 = false;
                                                Cliente.procesoCotizar(cliente, listVehiculo, opcV);
                                            }
                                        }
                                    }
                                }
                                if (opcAux.equals("2")){
                                    valopcAux = false;
                                    Main.menuCliente();
                                }
                            }
                        }
                    }else{
                        System.out.println("Actualmente el stock de vehículos " 
                                + "se encuentra vacío. Por favor, vuelva en otra ocasión."
                        );
                        Main.menuCliente();
                    }
                }
                //LE PERMITE VER EL ESTADO DE UNA SOLICITUD SI FUE ACEPTADA O RECHAZADA
                else if (opc.equals("2")){
                    valopc = false;
                    ArrayList<ArrayList> list = new ArrayList();
                    ArrayList<Integer> cotizacion = new ArrayList();
                    list = cliente.getSolicitud();
                    Object obj; String estado = ""; String motivo = "";
                    String tpSolicitud = "";
                    int idSoli = 0;
                    Vehiculo v = new Vehiculo("","",0,"","",0,0);
                    for (int i = 0; i < list.size(); i++){
                        ArrayList<Object> soli = new ArrayList();
                        soli = list.get(i);
                        for (int k = 0; k < soli.size(); k ++){
                            if (k == 0){
                                obj = soli.get(k);
                                estado = (String)obj;
                            }
                            if (k == 1){
                                obj = soli.get(k);
                                motivo = (String)obj;
                            }
                            if (k == 2){
                                obj = soli.get(k);
                                v = (Vehiculo)obj;
                            }
                            if (k == 3){
                                obj = soli.get(k);
                                tpSolicitud = (String)obj;
                            }
                            if (tpSolicitud.equals("Cotizacion")){
                                if (!cotizacion.contains(i)){
                                    cotizacion.add(i);
                                }
                                idSoli = cotizacion.size();
                                System.out.print("--------------------------------------------------" + "\n"
                                + "Respuesta Solicitud Cotización" + "(" + idSoli + ")" + "\n"
                                + "Estado: " + estado + "\n"
                                + "Motivo: " + motivo + "\n"
                                + "Tipo de solicitud: " + tpSolicitud + "\n"
                                + v
                                + "--------------------------------------------------" + "\n");
                                tpSolicitud = "";
                            }
                        }
                    }
                    if (cotizacion.size() == 0){
                        System.out.println("No hay respuesta de solicitud!!!");
                        Main.menuCliente();
                    }else{
                        boolean validar = true;
                        String op = "";
                        while(validar){
                            System.out.print("--------------------------------------------------" + "\n"
                            + "1.- Solicitar Compra" + "\n" 
                            + "2.- Descartar Cotización" + "\n"
                            + "3.- Regresar" + "\n" 
                            + "--------------------------------------------------" + "\n"
                            + "Por favor, digite el número de la acción que desea realizar: ");
                            op = textoIngresado.nextLine();
                            if (Main.validarOpcion(op) == true){
                                if (op.equals("1")){
                                    validar = false;
                                    String opcV;
                                    boolean valopcAux1 = true;
                                    while(valopcAux1){
                                        System.out.print("De la lista de cotizaciones, digite el número del que desea comprar: ");
                                        opcV = textoIngresado.nextLine();
                                        if (Main.validarOpcion(opcV) == true){
                                            int o = Integer.parseInt(opcV);
                                            if(o <= cotizacion.size()){
                                                valopcAux1 = false;
                                                o = o - 1;
                                                o = cotizacion.get(o);
                                                Object obj1 = list.get(o).get(0);
                                                Object obj3 = list.get(o).get(1);
                                                String estate = (String)obj1;
                                                String mtv = (String)obj3;
                                                if (estate.equals("Aceptada")){
                                                    Object obj2 = list.get(o).get(2);
                                                    Vehiculo vc = (Vehiculo)obj2;
                                                    vc.setEstado("Solicitado");
                                                    Cliente.procesoComprar(cliente, vc, opcV, o);
                                                }else{
                                                    System.out.print("--------------------------------------------------" + "\n"
                                                    + "La solicitud seleccionada fue Rechazada, " + "\n" 
                                                    + "por lo que no está permitdo solictar compra. Motivo: " + mtv + "\n" 
                                                    + "--------------------------------------------------" + "\n");
                                                    Main.menuCliente();
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (op.equals("2")){
                                    validar = false;
                                    String opcV;
                                    boolean valopcAux1 = true;
                                    while(valopcAux1){
                                        System.out.print("De la lista de cotizaciones, digite el número del que desea descartar: ");
                                        opcV = textoIngresado.nextLine();
                                        if (Main.validarOpcion(opcV) == true){
                                            int o = Integer.parseInt(opcV);
                                            if(o <= cotizacion.size()){
                                                valopcAux1 = false;
                                                o = o - 1;
                                                o = cotizacion.get(o);
                                                cliente.removerSolicitud(o);
                                                System.out.println("Solicitud descartada satisfactoriamente!!!");
                                                Main.menuCliente();
                                            }
                                        }
                                    }
                                }
                                else if (op.equals("3")){
                                    validar = false;
                                    Main.menuCliente();
                                }else{
                                    System.out.println("******************************************************************************" 
                                    + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                                }
                            }
                        }
                    }
                }
                //PUEE VER EL ESTADO DE UNA COMPRA SI FUE ACEPTADA O RECHAZADA
                else if (opc.equals("3")){
                    valopc = false;
                    ArrayList<ArrayList> list = new ArrayList();
                    list = cliente.getSolicitud();
                    Object obj; String estado = ""; String motivo = "";
                    String tpSolicitud = "";
                    int idSoliCompra = 0;
                    Vehiculo v = new Vehiculo("","",0,"","",0,0);
                    for (int i = 0; i < list.size(); i++){
                        ArrayList<Object> soli = new ArrayList();
                        soli = list.get(i);
                        for (int k = 0; k < soli.size(); k ++){
                            if (k == 0){
                                obj = soli.get(k);
                                estado = (String)obj;
                            }
                            if (k == 1){
                                obj = soli.get(k);
                                motivo = (String)obj;
                            }
                            if (k == 2){
                                obj = soli.get(k);
                                v = (Vehiculo)obj;
                            }
                            if (k == 3){
                                obj = soli.get(k);
                                tpSolicitud = (String)obj;
                            }
                            if (tpSolicitud.equals("Compra")){
                                idSoliCompra = idSoliCompra + 1;
                                System.out.print("--------------------------------------------------" + "\n"
                                + "Respuesta Solicitud Compra" + "(" + idSoliCompra + ")" + "\n"
                                + "Estado: " + estado + "\n"
                                + "Motivo: " + motivo + "\n"
                                + "Tipo de solicitud: " + tpSolicitud + "\n"
                                + v + "\n");
                                if (estado.equals("Aceptada")){
                                    System.out.println("La compra se realizó satisfactoriamente!!!. " + "\n"
                                    + "Puede acercarse al taller de la concesionaria a retirar el vehículo comprado "
                                    + "cuando usted guste, siempre y cuando sea dentro del horario laboral. " + "\n"
                                    + "Muchas gracias por su compra!");
                                }
                                System.out.print("--------------------------------------------------" + "\n");
                                tpSolicitud = "";
                            }
                        }
                    }
                    if (idSoliCompra != 0){
                        System.out.print("--------------------------------------------------" + "\n"
                        + "1.- Regresar" + "\n" 
                        + "--------------------------------------------------" + "\n"
                        + "Por favor, digite el número de la acción que desea realizar: ");
                        String opcAux;
                        opcAux = textoIngresado.nextLine();
                        while(!opcAux.equals("1")){
                            System.out.print("Por favor, digite correctamente el número para regresar: ");
                            opcAux = textoIngresado.nextLine();
                        }
                        Main.menuCliente();
                    }else{
                        System.out.println("No hay respuesta(s) de solicitud(s) de compra!!!");
                        Main.menuCliente();
                    }
                }
                //PUEDE SOLICITAR UN MANTENIMIENTO A LA CONCESIONARIA
                //SIEMPRE Y CUANDO EL VEHICULO HAYA SIDO ADQUIRIDO DENTRO DE LA CONCESIONARIA
                else if (opc.equals("4")){
                    valopc = false;
                    if (cliente.getNoVehiculoComprado() > 0){
                        ArrayList<ArrayList> list = new ArrayList();
                        ArrayList<Integer> cotizacion = new ArrayList();
                        list = cliente.getSolicitud();
                        Object obj; String estado = ""; String motivo = "";
                        String tpSolicitud = "";
                        int idCompra = 0;
                        Vehiculo v = new Vehiculo("","",0,"","",0,0);
                        for (int i = 0; i < list.size(); i++){
                            ArrayList<Object> soli = new ArrayList();
                            soli = list.get(i);
                            for (int k = 0; k < soli.size(); k ++){
                                if (k == 0){
                                    obj = soli.get(k);
                                    estado = (String)obj;
                                }
                                if (k == 1){
                                    obj = soli.get(k);
                                    motivo = (String)obj;
                                }
                                if (k == 2){
                                    obj = soli.get(k);
                                    v = (Vehiculo)obj;
                                }
                                if (k == 3){
                                    obj = soli.get(k);
                                    tpSolicitud = (String)obj;
                                }
                                if (tpSolicitud.equals("Compra") && estado.equals("Aceptada") && v.getEstado().equals("Comprado")){
                                    if (!cotizacion.contains(i)){
                                        cotizacion.add(i);
                                    }
                                    idCompra = cotizacion.size();
                                    System.out.print("--------------------------------------------------" + "\n"
                                    + "Vehículo Comprado" + "(" + idCompra + ")" + "\n"
                                    + "Estado: " + estado + "\n"
    //                                + "Motivo: " + motivo + "\n"
    //                                + "Tipo de solicitud: " + tpSolicitud + "\n"
                                    + v + "\n");
                                    System.out.print("--------------------------------------------------" + "\n");
                                }
                            }
                        }
                        if (idCompra != 0){
                            String opcAux;
                            boolean valida = true;
                            while(valida){                            
                                System.out.print("--------------------------------------------------" + "\n"
                                + "1.- Solicitar Mantenimiento Preventivo" + "\n" 
                                + "2.- Solicitar Mantenimiento de Emergencia" + "\n"         
                                + "3.- Regresar" + "\n" 
                                + "--------------------------------------------------" + "\n"
                                + "Por favor, digite el número de la acción que desea realizar: ");
                                opcAux = textoIngresado.nextLine();
                                if (opcAux.equals("1")){
                                    valida = false;
                                    String opcV;
                                    boolean valopcAux1 = true;
                                    while(valopcAux1){
                                        System.out.print("De la lista de vehículos, digite el número del que desea realizar un mantenimiento preventivo: ");
                                        opcV = textoIngresado.nextLine();
                                        if (Main.validarOpcion(opcV) == true){
                                            int op = Integer.parseInt(opcV);
        //                                        System.out.println("el" + op + "-" + listVehiculo.size());
                                            if(op <= idCompra){
                                                op = op - 1;
                                                op = cotizacion.get(op);
                                                valopcAux1 = false;
                                                System.out.print("Por favor, ingrese el kilometraje de su vehículo[km(s) en número(s)]: ");
                                                String km = textoIngresado.nextLine();
                                                if (Main.validarOpcion(km)){
                                                    procesoMantenimiento(cliente, list, "Preventivo", op, Integer.parseInt(km));
                                                }
                                                
                                            }
                                        }
                                    }

                                }else if (opcAux.equals("2")){
                                    valida = false;
                                    String opcV;
                                    boolean valopcAux1 = true;
                                    while(valopcAux1){
                                        System.out.print("De la lista de vehículos, digite el número del que desea realizar un mantenimiento de emergencia: ");
                                        opcV = textoIngresado.nextLine();
                                        if (Main.validarOpcion(opcV) == true){
                                            int op = Integer.parseInt(opcV);
        //                                        System.out.println("el" + op + "-" + listVehiculo.size());
                                            if(op <= idCompra){
                                                op = op - 1;
                                                op = cotizacion.get(op);
                                                valopcAux1 = false;
                                                System.out.print("Por favor, ingrese el kilometraje de su vehículo[km(s) en número(s)]: ");
                                                String km = textoIngresado.nextLine();
                                                if (Main.validarOpcion(km)){
                                                    procesoMantenimiento(cliente, list, "Emergencia", op, Integer.parseInt(km));
                                                }
                                            }
                                        }
                                    }
                                }else if (opcAux.equals("3")){
                                    valida = false;
                                    Main.menuCliente();
                                }else{
                                    System.out.println("******************************************************************************" 
                                        + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                                }
                            }
                            Main.menuCliente();
                        }else{
                            System.out.println("No hay vehículos disponibles para realizarle un mantenimiento!!!");
                            Main.menuCliente();
                        }
                    }else{
                        System.out.println("Lo sentimos, el servicio de mantenimiento de la concesionaria "
                        + "son para aquellos clientes que ya han adquirido al menos un vehículo!!!.");
                        Main.menuCliente();
                    }
                }
                else if (opc.equals("5")){
                    //REVISAR RESPUESTA DE SOLICITUD DE MANTENIMIENTO
                    //SABER SI FUE ACEPTADA O REHAZADA
                    valopc = false;
                    ArrayList<ArrayList> list = new ArrayList();
                    list = cliente.getSolicitud();
                    Object obj; String estado = ""; String motivo = "";
                    String tpSolicitud = "";
                    int idSoliCompra = 0;
                    Vehiculo v = new Vehiculo("","",0,"","",0,0);
                    for (int i = 0; i < list.size(); i++){
                        ArrayList<Object> soli = new ArrayList();
                        soli = list.get(i);
                        for (int k = 0; k < soli.size(); k ++){
                            if (k == 0){
                                obj = soli.get(k);
                                estado = (String)obj;
                            }
                            if (k == 1){
                                obj = soli.get(k);
                                motivo = (String)obj;
                            }
                            if (k == 2){
                                obj = soli.get(k);
                                v = (Vehiculo)obj;
                            }
                            if (k == 3){
                                obj = soli.get(k);
                                tpSolicitud = (String)obj;
                            }
                            if (tpSolicitud.equals("Mantenimiento") && estado.equals("Aceptada") && v.getEstado().equals("Admitido")){
                                idSoliCompra = idSoliCompra + 1;
                                System.out.print("--------------------------------------------------" + "\n"
                                + "Estado Solicitud Mantenimiento" + "(" + idSoliCompra + ")" + "\n"
                                + "Estado: " + estado + "\n"
                                + "Tipo de solicitud: " + tpSolicitud + "\n"
                                + v + "\n");
                                System.out.print("--------------------------------------------------" + "\n");
                                tpSolicitud = "";
                            }
                        }
                    }
                    if (idSoliCompra != 0){
                        System.out.print("--------------------------------------------------" + "\n"
                        + "1.- Regresar" + "\n" 
                        + "--------------------------------------------------" + "\n"
                        + "Por favor, digite el número de la acción que desea realizar: ");
                        String opcAux;
                        opcAux = textoIngresado.nextLine();
                        while(!opcAux.equals("1")){
                            System.out.print("Por favor, digite correctamente el número para regresar: ");
                            opcAux = textoIngresado.nextLine();
                        }
                        Main.menuCliente();
                    }else{
                        System.out.println("No hay respuesta(s) de solicitud(s) de Mantenimiento!!!");
                        Main.menuCliente();
                    }
                }
                else if (opc.equals("6")){
                    //REVISAR PROGRESO DEL MANTENIMIENTO 
                    //LE PERMITE VER SI ESTA AUN EN REPARACION 
                    //O YA SE ENCUENTRA EN PRUEBA
                    valopc = false;
                    int noMantenimiento = 0;
                    ArrayList<ArrayList> listManPreventivo = new ArrayList();
                    ArrayList<ArrayList> listManEmergencia = new ArrayList();
                    listManPreventivo = Mantenimiento.getListMantenimientoPreventivo();
                    listManEmergencia = Mantenimiento.getListMantenimientoEmergencia();
                    int distancia = 0; String progreso = ""; float costo = 0;
                    Vehiculo vehi = new Vehiculo("", "", 0, "", "", 0, 0);
                    for (ArrayList<Object> list : listManPreventivo){
                        for (int i = 0; i < list.size(); i++){
                            if (i == 1){ 
                                vehi = (Vehiculo)list.get(i);
                            }
                            if (i == 2){
                                distancia = (int)list.get(i);
                            }
                            if (i == 3){
                                progreso = (String)list.get(i);
                            }
                            if (i == 4){
                                costo = (float)list.get(i);
                            }
                        }
                        if (!progreso.equals("Finalizado")){
                            noMantenimiento = noMantenimiento + 1;
                            System.out.println("Mantenimiento Preventivo(" + noMantenimiento + ")" + "\n"
                            + "Distancia: " + distancia + "\n"
                            + "Costo: $" + costo + " dólares" + "\n"
                            + "Progreso: " + progreso + "\n"
                            + vehi.toString()); 
                        }
                    }
                    for (ArrayList<Object> list : listManEmergencia){
                        for (int i = 0; i < list.size(); i++){
                            if (i == 1){ 
                                vehi = (Vehiculo)list.get(i);
                            }
                            if (i == 2){
                                distancia = (int)list.get(i);
                            }
                            if (i == 3){
                                progreso = (String)list.get(i);
                            }
                            if (i == 5){
                                costo = (float)list.get(i);
                            }
                        }
                        if (!progreso.equals("Finalizado")){
                            noMantenimiento = noMantenimiento + 1;
                        System.out.println("Mantenimiento Emergencia(" + noMantenimiento + ")" + "\n"
                        + "Distancia: " + distancia + "\n"
                        + "Costo: $" + costo + " dólares" + "\n"
                        + "Progreso: " + progreso + "\n"
                        + vehi.toString()); 
                        }
                    }
                    if (noMantenimiento != 0){
                        System.out.print("--------------------------------------------------" + "\n"
                        + "1.- Regresar" + "\n" 
                        + "--------------------------------------------------" + "\n"
                        + "Por favor, digite el número de la acción que desea realizar: ");
                        String opcAux;
                        opcAux = textoIngresado.nextLine();
                        while(!opcAux.equals("1")){
                            System.out.print("Por favor, digite correctamente el número para regresar: ");
                            opcAux = textoIngresado.nextLine();
                        }
                        Main.menuCliente();
                    }else{
                        System.out.println("No hay mantenimiento(s) en progreso!!!");
                        Main.menuCliente();
                    }
                }
                //PROCESO PARA REGREAR AL MENU INICIAL COMO LO ES EL MUNI DE INICIO DE SISTEMA
                else if (opc.equals("7")){
                    valopc = false;
                    Main.procesoInicial();
                }
                //PROCESO PARA SALIR DEL SISTEMA COMPLETAMENTE
                else if (opc.equals("8")){
                    Proceso.grabarDatos();
                    System.exit(0);
                }
                else{
                    valopc = false;
                    Main.menuCliente();
                }
            }else{
                System.out.println("******************************************************************************" 
                + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                Main.menuCliente();
            }
        }
    }
    
}
