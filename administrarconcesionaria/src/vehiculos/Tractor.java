/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author Carlos
 */
public class Tractor extends Vehiculo{
    
    //ATRIBUTOS
    private boolean usoAgricola;
    private String tipoTransmision;
    
    //CONSTRUCTOR
    public Tractor(){
        super("", "", 0, "", "", 0, 0);
    }
    public Tractor(String marca, String modelo, int añoFabricacion, String estado, String motor, int noLLantas, float precio, boolean usoAgricola, String tipoTransmision){
        super(marca, modelo, añoFabricacion, estado, motor, noLLantas, precio);
        this.usoAgricola = usoAgricola;
        this.tipoTransmision = tipoTransmision;
    }
    
    //GETTERS
    public boolean getUsoAgricola(){
        return this.usoAgricola;
    }
    public String getTipoTransmision(){
        return this.tipoTransmision;
    }
    
    //SETTERS
    public void setUsoAgricola(boolean usoAgricola){
        this.usoAgricola = usoAgricola;
    }
    public void setTipoTransmision(String tipoTransmision){
        this.tipoTransmision = tipoTransmision;
    }
    
    //METODOS
    @Override
    //Presentar la informacion de tractor
    public String toString(){
        return ("-----=====Tractor=====-----" + "\n" +
                "Marca: " + this.getMarca() + 
                ", Modelo: " +  this.getModelo() +
                ", Año Fabricación: " + this.getAñoFabricacion() + 
                ", Estado: " + this.getEstado() + 
                ", Motor: " + this.getMotor() + 
                ", No. LLantas: " + this.getNoLLantas() + 
                ", Precio: " + this.getPrecio() + 
                ", Uso Agrícola: " + this.usoAgricola + 
                ", Tipo de Transmisión: " + this.tipoTransmision + 
                "\n");
    }
}
