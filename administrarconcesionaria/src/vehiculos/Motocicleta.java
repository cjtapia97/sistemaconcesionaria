/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author Carlos
 */
public class Motocicleta extends Vehiculo{
    
    //ATRIBUTOS
    private String categoria;
    
    //CONSTRUCTOR
    public Motocicleta(){
        super("", "", 0, "", "", 0, 0);
    }
    public Motocicleta(String marca, String modelo, int añoFabricacion, String estado, String motor, int noLLantas, float precio, String categoria){
        super(marca, modelo, añoFabricacion, estado, motor, noLLantas, precio);
        this.categoria = categoria;
    }
    
    //GETTERS
    public String getCategoria(){
        return this.categoria;
    }
    
    //SETTERS
    public void setCategoria(String categoria){
        this.categoria = categoria;
    }
    
    //METODOS
    @Override
    //Presentar la informacion de motocicleta
    public String toString(){
        return ("-----=====Motocicleta=====-----" + "\n" +
                "Marca: " + this.getMarca() + 
                ", Modelo: " +  this.getModelo() +
                ", Año Fabricación: " + this.getAñoFabricacion() + 
                ", Estado: " + this.getEstado() + 
                ", Motor: " + this.getMotor() + 
                ", No. LLantas: " + this.getNoLLantas() + 
                ", Precio: " + this.getPrecio() + 
                ", Categoria: " + this.categoria + 
                "\n");
    }
}
