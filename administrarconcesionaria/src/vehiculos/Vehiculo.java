/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;
import sistema.SistemaConcesionaria;

/**
 *
 * @author Carlos
 */
public class Vehiculo extends SistemaConcesionaria{
    
    //ATRIBUTOS
    private String marca;
    private String modelo;
    private int añoFabricacion;
    private String estado;
    private String motor;
    private int noLLantas;
    private float precio;
    
    //CONSTRUCTOR
    public Vehiculo(String marca, String modelo, int añoFabricacion, String estado, String motor, int noLLantas, float precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.añoFabricacion = añoFabricacion;
        this.estado = estado;
        this.motor = motor;
        this.noLLantas = noLLantas;
        this.precio = precio;
    }
    
    //GETTERS
    public String getMarca() {
        return this.marca;
    }
    public String getModelo() {
        return this.modelo;
    }
    public int getAñoFabricacion() {
        return this.añoFabricacion;
    }
    public String getEstado() {
        return this.estado;
    }
    public String getMotor() {
        return this.motor;
    }
    public int getNoLLantas() {
        return this.noLLantas;
    }
    public float getPrecio(){
        return this.precio;
    }
    
    //SETTERS
    public void setMarca(String marca) {
        this.marca = marca;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    public void setAñoFabricacion(int añoFabricacion) {
        this.añoFabricacion = añoFabricacion;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public void setMotor(String motor) {
        this.motor = motor;
    }
    public void setNoLLantas(int noLLantas) {
        this.noLLantas = noLLantas;
    }
    public void setPrecio(float precio){
        this.precio = precio;
    }
    
    //METODOS
    
    

    
    
    
}
