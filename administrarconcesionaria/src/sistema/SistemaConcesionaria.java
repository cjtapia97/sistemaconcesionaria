/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import java.util.ArrayList;
import usuarios.*;
import vehiculos.*;

/**
 *
 * @author Carlos
 */
public abstract class SistemaConcesionaria {
    //ESTA CLASE SE LA USO COMO BASE DE DATOS PUESTO QU ELA MAYORIA OBJETOS DEL SISTEMA SON GUARDADOS EN LAS SIGUIENTES LISTAS
    //ATRIBUTOS
    private static ArrayList<Cliente> listCliente = new ArrayList();
    private static ArrayList<Vendedor> listVendedor = new ArrayList();
    private static ArrayList<JefeTaller> listJefeTaller = new ArrayList();
    private static ArrayList<Supervisor> listSupervisor = new ArrayList();    
    private static ArrayList<String> listUsuariosRegistrados = new ArrayList(); 
    private static ArrayList<Automovil> listAutomovil = new ArrayList();
    private static ArrayList<Motocicleta> listMotocicleta = new ArrayList();
    private static ArrayList<Camion> listCamion = new ArrayList();
    private static ArrayList<Tractor> listTractor = new ArrayList();
    

    //METODOS PARA AGREGAR LA INFORMACION A SUS RESPECTIVOS METODOS
    public static void agregarListaCliente(Cliente cliente){
        listCliente.add(cliente);
    }
    public static void agregarListaVendedor(Vendedor vendedor){
        listVendedor.add(vendedor);
    }
    public static void agregarListaSupervisor(Supervisor supervisor){
        listSupervisor.add(supervisor);
    }
    public static void agregarListaJefeTaller(JefeTaller jefeTaller){
        listJefeTaller.add(jefeTaller);
    }
    public static void agregarUsuario(String datos){
        listUsuariosRegistrados.add(datos);
    }
    public static void agregarAutomovil(Automovil automovil){
        listAutomovil.add(automovil);
    }
    public static void agregarMotocicleta(Motocicleta motocicleta){
        listMotocicleta.add(motocicleta);
    }
    public static void agregarCamion(Camion camion){
        listCamion.add(camion);
    }
    public static void agregarTractor(Tractor tractor){
        listTractor.add(tractor);
    }
    
    //GETTERS
    public static ArrayList<Cliente> getListCliente() {
        return listCliente;
    }
    public static ArrayList<Vendedor> getListVendedor() {
        return listVendedor;
    }
    public static ArrayList<JefeTaller> getListJefeTaller() {
        return listJefeTaller;
    }
    public static ArrayList<Supervisor> getListSupervisor() {
        return listSupervisor;
    }
    public static ArrayList<String> getListUsuariosRegistrados(){
        return listUsuariosRegistrados;
    }
    public static ArrayList<Automovil> getListAutomovil(){
        return listAutomovil;
    }
    public static ArrayList<Motocicleta> getListMotocicleta(){
        return listMotocicleta;
    }
    public static ArrayList<Camion> getListCamion(){
        return listCamion;
    }
    public static ArrayList<Tractor> getListTractor(){
        return listTractor;
    }
    
    
    
    //METODO PARA SER SOBREESCRITO
    public ArrayList<Vehiculo> consultarStock(){
        return null;
    }
    
               
}
